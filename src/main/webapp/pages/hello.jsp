<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>${title}</title>
    <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/bootstrap-theme.min.css" />" rel="stylesheet">
</head>
<body role="document">
Feature: ${description}
<table class="table">
    <tr>
        <td>test1</td>
        <td>test2</td>
    </tr>
</table>
<script type="text/javascript" src="<c:url value="/pages/js/bootstrap.min.js" />"></script>
</body>
</html>