package ru.libs.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class WorkWithDB {

    public static void execute (String query, String connectionName) {
        DatabaseConnectionFactory dbFactory = DatabaseConnectionFactory.getDatabaseConnectionFactory();
        Connection connection;
        if (null == query || query.isEmpty())
            throw new Error("Query is empty.");

        connection = dbFactory.getConnection(connectionName);
        Statement statement;
        try {
            statement = connection.createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
