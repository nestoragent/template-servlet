package ru.servlet;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * Created by sbt-velichko-aa on 04.10.2016.
 */
public class MainServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //mustache
        PrintWriter out = resp.getWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        File template = new File(getServletContext().getRealPath("/pages/hello.html"));
        Mustache mustache = mf.compile(new InputStreamReader(new FileInputStream(template), Charset.forName("UTF-8")),
                template.getName());
        HashMap<String, String> scopes = new HashMap<>();
        scopes.put("resources", req.getContextPath());
        scopes.put("title", "Hello World");
        scopes.put("description", "Hello World");
        mustache.execute(out, scopes);

//        //jsp
//        req.setAttribute("title", "Hello World");
//        req.setAttribute("description", "Hello World");
//        req.getRequestDispatcher("/pages/hello.jsp").forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
